import gcip
from tests import conftest
from gcip.addons.python.jobs.linter import Flake8


def test():
    pipeline = gcip.Pipeline()
    pipeline.add_children(Flake8())

    conftest.check(pipeline.render())
