from gcip import Job, Pipeline, Sequence
from gcip.addons.gitlab.scripts import clone_repository


def get_build_deploy_sequence(environment: str):
    return Sequence().add_children(
        Job(stage="build", script=f"docker build -t myimage-{environment} ."),
        Job(stage="deploy", script=["docker login", f"docker push myimage-{environment}"]),
    )


pipeline = Pipeline()
pipeline.initialize_image("my/enterprise/build-image:stable")

for environment in ("develop", "test", "production"):
    jobs = get_build_deploy_sequence(environment)
    jobs.prepend_scripts(
        clone_repository(path="projectx/configuration", branch=environment),
        f"source {environment}.env",
    )
    jobs.add_tags(environment)

    pipeline.add_children(jobs, stage=environment)

pipeline.write_yaml("generated-config.yml")
